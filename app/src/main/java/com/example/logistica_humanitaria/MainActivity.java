

package com.example.logistica_humanitaria;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


import Database.DbEstados;
import Database.Estados;
import Database.Device;
import Database.ProcesosPHP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements Response.Listener<JSONObject>,Response.ErrorListener{

    private Button btnGuardar;
    private Button btnListar;
    private Button btnLimpiar;
    private Button btnCerrar;
    private EditText txtEstado;
    private Estados savedEstado;
    private int id;
    private String nombre;
    SharedPreferences sharedPreferences;
    private String serverip = "https://vaneos.000webhostapp.com/WebService/";
    JsonObjectRequest jsonObjectRequest;
    private RequestQueue request;
    ProcesosPHP php;
    DbEstados agenda;

    ArrayList<Estados> estados = new ArrayList<Estados>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.request = Volley.newRequestQueue(this);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnListar = (Button) findViewById(R.id.btnListar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        this.php=new ProcesosPHP();
        this.agenda = new DbEstados(this);
        php.setContext(this);
        txtEstado = (EditText) findViewById(R.id.edtEstado);
        sharedPreferences = getSharedPreferences("datos", Context.MODE_PRIVATE);
        boolean agregados = sharedPreferences.getBoolean("agregados",false);

        if(!agregados) {
            if(isNetworkAvailable()) {
                this.mostrarTodosWebService();
            }
        }

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()) {
                    boolean completo = true;
                    if (txtEstado.getText().toString().equals("")) {
                        txtEstado.setError("Introduce el nombre");
                        completo = false;
                    }
                    if (completo) {
                        final Estados nEstado = new Estados();
                        nEstado.setNombreEstado(txtEstado.getText().toString());
                        nEstado.setIdMovil(Device.getSecureId(MainActivity.this));

                        if (savedEstado == null) {

                            if(isNetworkAvailable())
                                php.insertarContactoWebService(nEstado);
                            agenda.openDatabase();
                            agenda.insertContacto(nEstado);
                            agenda.close();
                            Toast.makeText(getApplicationContext(), "Agregado", Toast.LENGTH_SHORT).show();
                            limpiar();
                        } else {
                            if (savedEstado.getNombreEstado().equals(txtEstado.getText().toString())) {
                                Intent i = new Intent(MainActivity.this, ListActivity.class);
                                startActivityForResult(i, 0);
                            } else {
                                new AlertDialog.Builder(MainActivity.this)
                                        .setTitle("Confirmar")
                                        .setMessage("¿Deseas Modificarlo?")
                                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int whichButton) {
                                                if(isNetworkAvailable())
                                                    php.actualizarContactoWebService(nEstado, nombre);
                                                agenda.openDatabase();
                                                agenda.updateContacto(nEstado,id);
                                                agenda.close();
                                                limpiar();
                                                Toast.makeText(getApplicationContext(), "Modificado", Toast.LENGTH_SHORT).show();
                                                Intent i = new Intent(MainActivity.this, ListActivity.class);
                                                startActivityForResult(i, 0);
                                            }
                                        })
                                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int whichButton) {
                                                Intent i = new Intent(MainActivity.this, ListActivity.class);
                                                startActivityForResult(i, 0);
                                            }
                                        }).show();
                            }
                        }
                    }
                }
                else
                    Toast.makeText(MainActivity.this, "No  hay Conexión", Toast.LENGTH_SHORT).show();

            }
        });

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, ListActivity.class);
                startActivityForResult(i, 0);
                limpiar();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limpiar();
            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (Activity.RESULT_OK == resultCode){
            Estados estado = (Estados) data.getSerializableExtra("estado");
            savedEstado = estado;
            id = estado.getId();
            this.nombre=estado.getNombreEstado();
            txtEstado.setText(estado.getNombreEstado());
        }
    }
    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnected();
    }
    public void mostrarTodosWebService(){
        String url = serverip + "wsConsultarTodos.php?";
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }


    public void limpiar(){
        txtEstado.setText("");
        txtEstado.setError(null);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
    }

    @Override
    public void onResponse(JSONObject response) {

        JSONArray json = response.optJSONArray("estados");
        try {
            DbEstados agenda = new DbEstados(MainActivity.this);
            agenda.openDatabase();
            Estados estado;
            if(json==null)
                return;
            for (int i = 0;i < json.length(); i++) {
                estado = new Estados();
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(i);
                estado.setId(jsonObject.optInt("id"));
                estado.setNombreEstado(jsonObject.optString("nombre"));
                estado.setIdMovil(jsonObject.optString("idMovil"));
                agenda.insertContacto(estado);
            }
            agenda.close();
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean("agregados", true);
            editor.commit();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}