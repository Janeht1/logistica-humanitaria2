package com.example.logistica_humanitaria;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import Database.DbEstados;
import Database.Estados;
import Database.ProcesosPHP;

public class ListActivity extends android.app.ListActivity {

    private DbEstados agendaEstados;
    private Button btnNuevo;
    ProcesosPHP php;
    private String nombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        this.btnNuevo = (Button)findViewById(R.id.btnNuevo);
        this.agendaEstados = new DbEstados(this);
        this.agendaEstados.openDatabase();
        this.php=new ProcesosPHP();
        php.setContext(this);
        ArrayList<Estados> contactos = agendaEstados.allContactos();
        MyArrayAdapter adapter = new MyArrayAdapter(this,R.layout.activity_estado, contactos);
        setListAdapter(adapter);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ListActivity.this,MainActivity.class);
                i.putExtra("id",0);
                startActivity(i);
                finish();
            }
        });
    }


    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnected();
    }
    class MyArrayAdapter extends ArrayAdapter<Estados> {
        Context context;
        int textViewResourceId;
        ArrayList<Estados> objects;
        public MyArrayAdapter(Context context, int textViewResourceId,ArrayList<Estados> objects){
            super(context, textViewResourceId, objects);
            this.context = context;
            this.textViewResourceId = textViewResourceId;
            this.objects = objects;
        }
        public View getView(final int position, View convertView, ViewGroup viewGroup){
            LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewResourceId, null);
            TextView lblNombre = (TextView)view.findViewById(R.id.lblNombreEstado);
            Button modificar = (Button)view.findViewById(R.id.btnModificar);
            Button borrar = (Button)view.findViewById(R.id.btnBorrar);
            lblNombre.setText(objects.get(position).getNombreEstado());
            borrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isNetworkAvailable()) {
                        new AlertDialog.Builder(ListActivity.this)
                                .setTitle("Confirmar")
                                .setMessage("¿Deseas Eliminarlo?")
                                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        php.borrarContactoWebService(objects.get(position).getNombreEstado());
                                        agendaEstados.openDatabase();
                                        agendaEstados.deleteContacto(objects.get(position).getId());
                                        agendaEstados.close();
                                        objects.remove(position);
                                        notifyDataSetChanged();
                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        Intent i = new Intent(ListActivity.this, MainActivity.class);
                                        startActivityForResult(i, 0);
                                    }
                                }).show();

                    }
                }
            });
            modificar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("estado",objects.get(position));
                    Intent i = new Intent();
                    i.putExtras(bundle);
                    setResult(Activity.RESULT_OK,i);
                    finish();
                }
            });
            return view;
        }
    }
}
