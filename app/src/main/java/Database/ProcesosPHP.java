package Database;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.logistica_humanitaria.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ProcesosPHP implements Response.Listener<JSONObject>,Response.ErrorListener {

    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;
    private ArrayList<Estados> contactos = new ArrayList<Estados>();
    private String serverip = "https://vaneos.000webhostapp.com/WebService/";
    private boolean consulta;

    public void setContext(Context context){
        request = Volley.newRequestQueue(context);
    }

    public void insertarContactoWebService(Estados e){
        this.consulta=false;
        String url = serverip + "wsAgregar.php?nombre="+e.getNombreEstado()+"&idMovil="+e.getIdMovil();
        url = url.replace(" ","%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }
    public void actualizarContactoWebService(Estados e, String nombre){
        this.consulta=false;
        String url = serverip + "wsActualizar.php?nombre="+nombre+"&nombress="+e.getNombreEstado();
        url = url.replace(" ","%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }
    public void borrarContactoWebService(String nombre){
        this.consulta=false;
        String url = serverip + "wsDesactivar.php?nombres="+nombre;
        url = url.replace(" ","%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }

    public ArrayList<Estados> mostrarTodosWebService(String idMovil)
    {
        this.consulta=true;
        String url = serverip + "wsConsultarTodos.php?idMovil="+idMovil;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
        return this.contactos;
    }
    public ArrayList<Estados> mostrarContactoWebService(int idMovil)
    {
        this.consulta=true;
        String url = serverip + "wsConsultarContacto.php?idMovil="+idMovil;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
        return this.contactos;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
    }

    @Override
    public void onResponse(JSONObject response) {
        if(this.consulta) {
            this.contactos.removeAll(this.contactos);
            Estados contacto = null;
            JSONArray json = response.optJSONArray("estados");
            try {
                for (int i = 0;json!=null && i < json.length(); i++) {
                    contacto = new Estados();
                    JSONObject jsonObject = null;
                    jsonObject = json.getJSONObject(i);
                    contacto.setId(jsonObject.optInt("id"));
                    contacto.setNombreEstado(jsonObject.optString("estado"));
                    contacto.setIdMovil(jsonObject.optString("idMovil"));
                    contactos.add(contacto);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }
}
